///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   May 1st 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

int nameLen = 6;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}



/* ====CAT EMPIRE FUNCTIONS==== */

/* PUBLIC FUNCTIONS */

bool CatEmpire::empty () {
	return topCat == nullptr;
}

void CatEmpire::addCat( Cat* newCat ) {
	assert(newCat != nullptr);

	newCat -> left = nullptr;
	newCat -> right = nullptr;
	
	if (topCat == nullptr) {
		topCat = newCat;
		return;
	}

	addCat(topCat, newCat);
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	int depth = 1;
	dfsInorderReverse(topCat, depth);	
}


void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder(topCat);
}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder(topCat);
}

void CatEmpire::catGenerations() const {
	bfs(topCat, 1);
}

void CatEmpire::getEnglishSuffix(int n) const{
		if ((n == 1)|| ((n > 20) && (n % 10 == 1))) {
			cout << n << "st";
		} else if((n == 2) || ((n > 20) && (n % 10 == 2))) {
			cout << n << "nd";
		} else if((n == 3) || ((n > 20) && (n % 10 == 3))) {
			cout << n << "rd";
		} else {
			cout << n << "th";
		}
}

/* PRIVATE FUNCTIONS */

void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {
	assert(atCat != nullptr);
	assert(newCat != nullptr);
	
	if (atCat -> name > newCat -> name) {
		if (atCat -> left == nullptr) {
			atCat -> left = newCat;
		} else {
			addCat(atCat -> left, newCat);
		}
	}

	if (atCat -> name < newCat -> name) {
		if (atCat -> right == nullptr) {
			atCat -> right = newCat;
		} else {
			addCat(atCat -> right, newCat);
		}
	}
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
	// if the node is nothing, return
	if (atCat == nullptr) {
		return;
	}
	
	// add to depth as we go down the tree
	depth ++;

	// go to the right and print correct amount of spaces with the name
	dfsInorderReverse(atCat -> right, depth);
	cout << string(nameLen * (depth - 1), ' ') << atCat -> name;

	// print statements based on if it is leaf node or the amount of children nodes 
	if (atCat -> left == nullptr && atCat -> right == nullptr) {
		cout << endl; 
	} else if (atCat -> left != nullptr && atCat -> right != nullptr) {
		cout << "<" << endl;
	} else if (atCat -> left != nullptr) {
		cout << "\\" << endl;
	} else if (atCat -> right != nullptr) {
		cout << "/" << endl;
	}
	
	// go to the left after going to the right
	dfsInorderReverse(atCat -> left, depth);
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
	// if the node is nothing, return
	if (atCat == nullptr) {
		return;
	}
	
	// go to the left first, print the name, then go to the right
	dfsInorder(atCat -> left);
	cout << atCat -> name << endl;
	dfsInorder(atCat -> right);
}

void CatEmpire::dfsPreorder( Cat* atCat ) const {
	// if the node is nothing, return
	if (atCat == nullptr) {
		return;
	}

	// print statements based the amount of children nodes 
	if (atCat -> left != nullptr && atCat -> right != nullptr) {
		cout << atCat -> name << " begat " << atCat -> left -> name << " and " << atCat -> right -> name << endl;
	} else if (atCat -> left != nullptr) {
		cout << atCat -> name << " begat " << atCat -> left -> name << endl;
	} else if (atCat -> right != nullptr) {
		cout << atCat -> name << " begat " << atCat -> right -> name << endl;
	} else if (atCat -> left == nullptr && atCat -> right == nullptr) {
		return;
	}

	// go to the left first, then the right
	dfsPreorder(atCat -> left);
	dfsPreorder(atCat -> right);
}

void CatEmpire::bfs(Cat* atCat, int depth) const {	
	// create two queues to hold the values
	queue<Cat*> tempq;
	queue<Cat*> tempq1;
	Cat* temp;
	// push the first node to the main queue
	tempq.push(atCat);
	
	// iterate through a list the cats using the two queues
	while (!tempq.empty()){	
		// gets and prints the generation
		getEnglishSuffix(depth); 
		cout<< " Generation"<<endl<<"  ";
		
		// going through and printing the name in the main queue
		while (!tempq.empty()){
			temp = tempq.front();
			tempq.pop();
			cout<<temp->name<<"  ";
			
			if(temp->left != nullptr)
				tempq1.push(temp->left);
			if(temp->right != nullptr)
				tempq1.push(temp->right);
		}

		//placing the values in the second queue in the main queue
		cout << endl;
		tempq = tempq1;
		
		// emptys out the second queue
		while(!tempq1.empty())
			tempq1.pop();
		
		// add to depth as we go down the tree
		depth++;
	}
}
